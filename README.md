# Editor-Oberfläche für squad.xml

Diese PHP-Anwendung dient dazu die squad.xml für Arma3 editieren zu können.

## Aktueller Funktionsumfang
- Anlegen von neuen Squad-Membern
- Aktualisieren von Squad-Membern
- Löschen von Squad-Membern
- Anzeige von Squad-Membern

## Voraussetzungen
- Webserver
- PHP
- .htaccess-Funktionalität (zum Absichern des Editors)
- htpasswd oder htpasswd2 zur Erzeugung von Zugangsdaten oder per  https://hostingcanada.org/htpasswd-generator/
- Das Programm (Webserver) muss im Dateisystem des Servers Schreibugriff auf die squad.xml haben

## Installation
- Im Webroot des Webservers die Daten dieses Repos ablegen (z.B. `git clone https://gitlab.com/chaapp/editsquad.git`)
- Die Daten sollten dann unter $webroot/editsquad/ liegen
- In Datei `editsquad.php` die Variable `$squadFilePath`auf den relativen oder absoluten Pfad zum editsquad.php setzen. (Wenn squad.xml z.B. im Webroot liegt: `../squad.xml)
- Schreibrechte für die squad.xml setzen (z.B. `chown www-data.www-data squad.xml`)
- Zugangsdaten für Absicherung erzeugen `htpasswd -c /$directory/passwords username` ($Directory ersetzen duruch ein Verzeichnis auf dem Server außerhalb des Webroots, oder https://hostingcanada.org/htpasswd-generator/)
- In Datei `.htpasswd` Pfad zur passwords-Datei bei `AuthUserFile` angeben.
- Backup der `squad.xml`machen