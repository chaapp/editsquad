<h1>Titan Platoon SquadXMl Updater</h1>

<form method="post">
	<label for="id">ID (Spieler-ID Arma3 Profil):</label><br>
	<input name="id" id="id" type="id"><br>
	<label for="nickname">Nickname (Spielername Arma3 Profil):</label><br>
	<input name="nickname" id="nickname" type="text"><br>
	<label for="name">Name (Real Name optional):</label><br>
	<input name="name" id="name" type="text"><br>
	<label for="email">Email: (optional)</label><br>
	<input name="email" id="email" type="text"><br>
	<label for="icq">ICQ: (optional)</label><br>
	<input name="icq" id="icq" type="text"><br>
	<label for="remark">Remark: (Anmerkung optional)</label><br>
	<input name="remark" id="remark" type="text"><br>	 	  
  	<br>
  	<input type="submit" name="submit" value="Update/Create">
	<input type="submit" name="delete" value="Delete">
	<input type="submit" name="showmembers" value=" show members">
	<br>
	<br>
	<label for="searchnick">Search for Nickname:</label><br>
	<input name="searchnick" id="searchnick" type="text"><br>
	<input type="submit" name="search" value="Search">		
</form>

<p>
Um einen neuen Eintrag anzulegen, bitte alle Felder ausfüllen (mindestens ID) und auf "Update/Create" klicken"<br/>
Zum Aktualisieren eines Eintrages bitte mindestens die ID eingeben und die zu aktualisierenden Felder, dann auf "Update/Create" klicken. Nicht ausgefüllte Felder werden nicht aktualisiert.<br/>
Um die ID eines Users zu finden entweder "Show Members" klicken oder per Suchfeld den ganzen Nickname eingeben (es werden aktuell nur Ergbnisse angezeigt, wenn der Nickname komplett richtig eingegeben wurde.)<br/>
Show members zeigt die gesamte Liste der User an. Man kann dann mit Strg+F im Browser nach Usern suchen.
Änderung sind sofort in der squad.xml live.<br/>
<a href=../squad.xml>Zeige squad.xml</a>
</p>
<?php

$squadFilePath = '../squad.xml';

if(isset($_POST['search'])) {

	$data=simplexml_load_file($squadFilePath);


	echo ("Ergebnisse:<br/>\n");

	foreach($data->member as $a) {

		if(strtolower($a->attributes()->nick) == strtolower($_POST['searchnick']))
		//if(stripos($a->attributes()->nick, $_POST['searchnick']) === TRUE )
		{
			echo ("------------------<br/>\n");
			echo nl2br("ID: ".(string)$a->attributes()->id."\r\n");
			echo nl2br("Nickname: ".(string)$a->attributes()->nick."\r\n");
			echo nl2br("Name: ".(string)$a->name."\r\n");
			echo nl2br("E-Mail: ".(string)$a->email."\r\n");
			echo nl2br("ICQ: ".(string)$a->icq."\r\n");
			echo nl2br("Remark: ".(string)$a->remark."\r\n");
		}

}

}

if(isset($_POST['showmembers'])) {

	showMembers();

}

if(isset($_POST['delete'])) {

	if($_POST['id'] == "") {
		exit("Es wurde keine ID eingegeben");
	
	}

	$data = simplexml_load_file("$squadFilePath");
	foreach ($data as  $member) {
		if ( $member->attributes()->id != $_POST['id']) {
			continue;
		}
		unset($member[0]);
		$handle=fopen("$squadFilePath","wb");
		fwrite($handle,$data->asXML());
		fclose($handle);
		break;
	}
	showMembers();

}

if(isset($_POST['submit'])) {
	$data=simplexml_load_file($squadFilePath);

echo nl2br($_POST['id']." wurde eingegeben<br/>\n");

if($_POST['id'] == "") {
	exit("Es wurde keine ID eingegeben");

}

$counter = 0;
foreach($data->member as $o) {
	if((string)$o->attributes()->id == $_POST['id']) {
	
		$counter = $counter + 1;		

	}
}



if($counter > 0) {
		
		echo nl2br("Eintrag vorhanden - aktualisiere<br/>\n");
		echo nl2br("");
                 foreach($data->member as $i) {

                         if((string)$i->attributes()->id == $_POST['id']) {

                                $i->attributes()->id = (string)$_POST['id'];
								if($_POST['nickname'] == "")
								{
									echo nl2br("Kein Nickname eingegeben - Feld wird nicht aktualisiert<br/>\n");
								}
								else{
									$i->attributes()->nick = (string)$_POST['nickname'];
								}
								if($_POST['name'] == "")
								{
									echo nl2br("Kein Name eingegeben - Feld wird nicht aktualisiert<br/>\n");
								}
								else{
									$i->name = $_POST['name'];
								}
								if($_POST['email'] == "")
								{
									echo nl2br("Keine E-Mail eingegeben - Feld wird nicht aktualisiert<br/>\n");
								}
								else{
									$i->email = $_POST['email'];
								}
								if($_POST['icq'] == "")
								{
									echo nl2br("Keine ICQ eingegeben - Feld wird nicht aktualisiert<br/>\n");
								}
								else{
									$i->icq = $_POST['icq'];
								}
								if($_POST['remark'] == "")
								{
									echo nl2br("Kein Remark eingegeben - Feld wird nicht aktualisiert<br/>\n");
								}
								else{
									$i->remark = $_POST['remark'];
								}
								
                        }

                }
		$counter = 0;
		
} else {
		echo ("-----------------------<br/>\n");
		echo nl2br("Eintrag nicht vorhanden - lege neu an<br>\n");
		echo nl2br("");
		
			$newMember = $data->addChild('member');
			$newMember->addAttribute('id', $_POST['id']);
			$newMember->addAttribute('nick', $_POST['nickname']);
            $newMember->addChild('name', $_POST['name']);
            $newMember->addChild('email', $_POST['email']);
            $newMember->addChild('icq', $_POST['icq']);
            $newMember->addChild('remark', $_POST['remark']);

}


$handle=fopen("$squadFilePath","wb");
fwrite($handle,$data->asXML());
fclose($handle);


showMembers();

}

function showMembers() {

global $squadFilePath;

$data=simplexml_load_file($squadFilePath);

foreach($data->member as $a) {
	echo ("------------------<br/>\n");
	echo nl2br("ID: ".(string)$a->attributes()->id."\r\n");
	echo nl2br("Nickname: ".(string)$a->attributes()->nick."\r\n");
	echo nl2br("Name: ".(string)$a->name."\r\n");
	echo nl2br("E-Mail: ".(string)$a->email."\r\n");
	echo nl2br("ICQ: ".(string)$a->icq."\r\n");
	echo nl2br("Remark: ".(string)$a->remark."\r\n");

}

}



?>
